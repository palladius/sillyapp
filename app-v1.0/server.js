'use strict';

const express = require('express');
const fs = require('fs');
const { execArgv } = require('process');


// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {

    try {
        const app_version = fs.readFileSync('./VERSION', 'utf8'); // eg, 1.0_20211221
        const myArray = app_version.split("_"); 
        const real_version = myArray[0]; // 1.0
        const version_date = myArray[1]; // 20211221

        console.log("[SillyApp] I just read from file the VERSION: "+app_version)

        // stdout for the app :)
        res.send(
          `<html><body>Hello World!<br/>
             -- sillyapp v.<b>${real_version}</b> <br/>
             (long version: <tt>${app_version}</tt>) <br/>
             
             <img style="display:inline" 
                  src="https://storage.googleapis.com/palladius-public/images/6573_1197702745901_4213289_n.jpg" 
                  heigth="100" />
          `); // change to File.read(VERSION)

      } catch (err) {
        console.error(err)
      }
      
    
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);