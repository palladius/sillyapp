'use strict';

const express = require('express');
const fs = require('fs')


// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
//const app_version = 
//const app_version = "TODO2";

// App
const app = express();
app.get('/', (req, res) => {

    try {
        const app_version = fs.readFileSync('./VERSION', 'utf8')
        console.log("I just read the VERSION: "+app_version)
        res.send('Wait - this HelloWorld is <b>BROKEN!!!</b><br/>\nsillyapp v'+app_version+''); // change to File.read(VERSION)

      } catch (err) {
        console.error(err)
      }
      
    
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);