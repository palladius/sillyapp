# sillyapp

A silly app to put on Public Docker to test deployments.
v1 is good, v2 is broken, v3 is good, and so on.
Docker container names will be well documented

The idea is to simply have this:

* `palladius/sillyapp:v1.0` which just works
* `palladius/sillyapp:v1.1` (symlink to palladius/sillyapp:v1.1_broken ) which is broken
* `palladius/sillyapp:v1.2` which just works

I don't know how to do it, I was thinking of maintaining THREE apps (v1=v3 and v2) in
different folders. What matters is not the source code, but the fact that you always
have available publicly (to demonstrate silly CICD pipelines) the 3 versions: you 
deploy v1, it works, yuou change your code to v2 - damn its broken - and then you either
rollback to v1 or commit to v3. V1 and V3 can be equivalent but UI needs to be distinguishible.

Optimization: I'll start with a / endpoint which always works (and just has BROKEN in bold ) 
and I might create a second /statusz which returns 200 or 500 depending on version 1/3 vs 2.

## INSTALL

docker build works, I haven't pushed to my repo yet.

Docker container is already pushed (manually) to https://hub.docker.com/r/palladius/sillyapp/tags

* TODO: push it with GitLab!
* `docker run -it -p 8080:8080 palladius/sillyapp:v1.0`

## License

For open source projects, say how it is licensed.
